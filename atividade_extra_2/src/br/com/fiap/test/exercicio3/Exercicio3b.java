package br.com.fiap.test.exercicio3;

import br.com.fiap.exercicio3.Cachorro;
import br.com.fiap.exercicio3.ChowChow;

public class Exercicio3b {
	public static void main(String[] args) {
		Cachorro cachorro = new Cachorro();
		Boolean valor = cachorro instanceof ChowChow;
		System.out.println("Verdadeiro ou Falso: "+valor);
	}
	
	//RESPOSTA: retorna FALSOSSSSSSSSSS
}