package br.com.fiap.test.exercicio3;

import br.com.fiap.exercicio3.Cachorro;
import br.com.fiap.exercicio3.ChowChow;

public class Exercicio3a {
	public static void main(String[] args) {
		ChowChow chowChow = new ChowChow();
		Boolean valor = chowChow instanceof Cachorro;
		System.out.println("Verdadeiro ou Falso: "+valor);
	}
	
	//RESPOSTA: retorna VERDADEIRO
}