package br.com.fiap.test;

import javax.swing.JOptionPane;

import br.com.fiap.CalculadoraBasicaImpl;

public class Exercicio1 {
	
	public static void main(String[] args) {
		
		String op1String = JOptionPane.showInputDialog("Digite o primeiro número da operação");
		String op2String = JOptionPane.showInputDialog("Digite o segundo número da operação");
		
		float op1 = Float.parseFloat(op1String);
		float op2 = Float.parseFloat(op2String);
		
		CalculadoraBasicaImpl calculadoraBasica = new CalculadoraBasicaImpl();
		float soma = calculadoraBasica.somar(op1, op2);
		float subtracao = calculadoraBasica.subtrair(op1, op2);
		float multiplicacao = calculadoraBasica.multiplicar(op1, op2);
		float divisao = calculadoraBasica.dividir(op1, op2);
		
		System.out.println("Soma: ".concat(String.valueOf(soma)));
		System.out.println("Subtração: ".concat(String.valueOf(subtracao)));
		System.out.println("Multiplicação: ".concat(String.valueOf(multiplicacao)));
		System.out.println("Divisão: ".concat(String.valueOf(divisao)));
	}
}
