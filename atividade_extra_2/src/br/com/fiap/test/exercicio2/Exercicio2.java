package br.com.fiap.test.exercicio2;

import br.com.fiap.exercicio2.Equipamento;
import br.com.fiap.exercicio2.EquipamentoEletronico;
import br.com.fiap.exercicio2.Movel;
import br.com.fiap.exercicio2.Smartphone;
import br.com.fiap.exercicio2.Tablet;

public class Exercicio2 {
	public static void main(String[] args) {
		EquipamentoEletronico tablet = new Tablet();
		EquipamentoEletronico smartphone = new Smartphone();
		Equipamento equipamento = new Equipamento();
		Object objeto = new Object();
		Movel movel = new Movel();
		Tablet tablet2 = new Tablet();
		Smartphone smartphone2 = new Smartphone();
		
	}
	
	/*
	 * RESPOSTAS:
	 * a) objeto para equipamento (objeto=equipamento;); 
	 * --> Implícita
	 * 
	 * b) objeto para móvel (objeto=movel;); 
	 * --> Implícita
	 * 
	 * c) equipamento para tablet2 (equipamento=tablet2;); 
	 * --> Implícita
	 * 
	 * d) equipamento para smartphone2 (equipamento=smartphone2;);
	 * --> Implícita
	 * 
	 * e) smartphone para tablet (smartphone=tablet;); 
	 * --> Implícita
	 * 
	 * f) smartphone2 para tablet2 (smartphone2=tablet2;); 
	 * Impossível
	 * 
	 * g) equipamento para tablete (equipamento=(Equipamento) tablet;); 
	 * Impossível pois a f dá erro de compilação
	 * 
	 * h) movel para tablete (movel=(Movel) tablet;); 
	 * Impossível pois a f dá erro de compilação
	 * 
	 * i) tablet para equipamento (tablet=(EquipamentoEletronico) equipamento;); 
	 * Impossível pois a f dá erro de compilação e essa mesma também dá
	 */
}