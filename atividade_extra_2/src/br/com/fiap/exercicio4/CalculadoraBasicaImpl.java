package br.com.fiap.exercicio4;

public class CalculadoraBasicaImpl implements CalculadoraBasica {

	@Override
	public float somar(float op1, float op2) throws Exception{
		float soma = op1 + op2;
		return soma;
	}

	@Override
	public float subtrair(float op1, float op2) throws Exception{
		
		float subtracao = op1 - op2;
		return subtracao;
	}

	@Override
	public float dividir(float op1, float op2) throws Exception{
		
		float divisao = op1 / op2;
		return divisao;
	}

	@Override
	public float multiplicar(float op1, float op2) throws Exception{
		
		float multiplicacao = op1 * op2;
		return multiplicacao;
	}

}
