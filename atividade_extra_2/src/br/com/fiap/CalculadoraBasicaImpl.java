package br.com.fiap;

public class CalculadoraBasicaImpl implements CalculadoraBasica {

	@Override
	public float somar(float op1, float op2) {
		float soma = op1 + op2;
		return soma;
	}

	@Override
	public float subtrair(float op1, float op2) {
		
		float subtracao = op1 - op2;
		return subtracao;
	}

	@Override
	public float dividir(float op1, float op2) {
		
		float divisao = op1 / op2;
		return divisao;
	}

	@Override
	public float multiplicar(float op1, float op2) {
		
		float multiplicacao = op1 * op2;
		return multiplicacao;
	}

}
